package com.example.blusmart.main.model;

import java.util.List;

class ModelForDutyList {

    private List<DutyListBean> duty_list;

    public List<DutyListBean> getToday_orders() {
        return duty_list;
    }

    public void setToday_orders(List<DutyListBean> duty_list) {
        this.duty_list = duty_list;
    }

    static class  DutyListBean {
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        String id ;


    }
}
