package com.example.blusmart.main.persenter;


import com.example.blusmart.app.AppController;
import com.example.blusmart.main.view.DutyListView;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class DutyListPersenterMvp implements DutyListPersenter {
    private final DutyListView dutyListView;

    public DutyListPersenterMvp(DutyListView dutyListView)
    {
        this.dutyListView=dutyListView;
    }
    @Override
    public void getDutyList(String checksum) {
          AppController.getInstance().getRetrofitServices().getDutyList("no-cache",checksum)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AppController.getInstance().defaultSubscribeScheduler())
                .subscribe(new Subscriber< Object  >() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        dutyListView.getDutyListError(e.getMessage());
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext( Object   response) {
                    AppController.getInstance().hideProgressDialog();
                    dutyListView.getDutyListSuccess(response);
                    }
                });
    }

    @Override
    public void showProgressBar() {
        dutyListView.showProgressBar();
    }
}
