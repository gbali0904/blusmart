package com.example.blusmart.main.view;

public interface DutyListView {

    void getDutyListSuccess(Object modelForDutyList);

    void getDutyListError(String message);

    void showProgressBar();
}
