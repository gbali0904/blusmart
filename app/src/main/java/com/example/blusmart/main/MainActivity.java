package com.example.blusmart.main;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.blusmart.R;
import com.example.blusmart.app.AppController;
import com.example.blusmart.dutydetail.DutyDetailFragment;
import com.example.blusmart.main.adapter.DutyListAdapter;
import com.example.blusmart.main.persenter.DutyListPersenterMvp;
import com.example.blusmart.main.view.DutyListView;
import com.example.blusmart.utility.Constants;
import com.example.blusmart.utility.DialogUtils;
import com.example.blusmart.utility.FragmentManagerUtil;
import com.example.blusmart.utility.GenrateChecksum;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements DutyListView {
    @BindView(R.id.dutyList)
    RecyclerView recy_dutyList;
    @BindView(R.id.content_layout)
    CoordinatorLayout Layoutcontent;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    ConstraintLayout container;
    private DutyListPersenterMvp mPresenter;
    private ArrayList dutyListData = new ArrayList<>();
    private DutyListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setupRecyclerView();
        mPresenter = new DutyListPersenterMvp(this);
        mPresenter.showProgressBar();
        mPresenter.getDutyList(GenrateChecksum.checksumForList());
        setToolBarTitle("All Duties");
    }

    private void setToolBarTitle(String barTitle) {
        getSupportActionBar().setTitle("");
        title.setText(barTitle);
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        adapter = new DutyListAdapter();
        adapter.setOnItemClickListener(new DutyListAdapter.ClickListener() {
            @Override
            public void onItemClick(String id) {
                DutyDetailFragment dutyDetailFragment = new DutyDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.DUTY_ID, id);
                dutyDetailFragment.setArguments(bundle);
                FragmentManagerUtil.replaceFragment(getSupportFragmentManager(), R.id.content_layout,
                        dutyDetailFragment, true, DutyDetailFragment.TAG);
                setToolBarTitle("Duty Detail");
            }
        });
        recy_dutyList.setLayoutManager(linearLayoutManager);
        recy_dutyList.setAdapter(adapter);
    }

    @Override
    public void getDutyListSuccess(Object obt) {
        AppController.getInstance().hideProgressDialog();
        dutyListData = (ArrayList) obt;
        DutyListAdapter adapter = (DutyListAdapter) recy_dutyList.getAdapter();
        adapter.setTracks(dutyListData);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void getDutyListError(String message) {
        AppController.getInstance().hideProgressDialog();
        DialogUtils.showDialog(this, message).show();
    }

    @Override
    public void showProgressBar() {
        AppController.getInstance().showProgressDialog(this);
    }
}
