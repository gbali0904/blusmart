package com.example.blusmart.dutydetail;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.blusmart.R;
import com.example.blusmart.app.AppController;
import com.example.blusmart.dutydetail.model.ModelForDutyDetail;
import com.example.blusmart.dutydetail.persenter.DutyDetailPersenterMvp;
import com.example.blusmart.dutydetail.view.DutyDetailView;
import com.example.blusmart.utility.Constants;
import com.example.blusmart.utility.DialogUtils;
import com.example.blusmart.utility.GenrateChecksum;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import im.delight.android.location.SimpleLocation;

public class DutyDetailFragment extends Fragment implements DutyDetailView {

    public static final String TAG = DutyDetailFragment.class.getSimpleName();
    @BindView(R.id.txtforassigned)
    TextView txtforassigned;
    @BindView(R.id.txtforid)
    TextView txtforid;
    @BindView(R.id.txtforstate)
    TextView txtforstate;
    @BindView(R.id.txtfortype)
    TextView txtfortype;
    @BindView(R.id.layUpdate)
    LinearLayout layUpdate;
    private Unbinder mUnbinder;
    private String id;
    private DutyDetailPersenterMvp detailPersenterMvp;
    private String action = "";
    private long timestamp;
    private double latitude;
    private double longitude;
    private SimpleLocation location;
    private String assigned;
    private String type;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_duty_detail, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        Bundle bundle = this.getArguments();
        id = bundle.getString(Constants.DUTY_ID, "");
        detailPersenterMvp = new DutyDetailPersenterMvp(this);
        detailPersenterMvp.showProgressBar();
        detailPersenterMvp.getDutyDetail(id, GenrateChecksum.checksumForDutyId(id));
        // construct a new instance of SimpleLocation
        location = new SimpleLocation(getActivity());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void getDutyDetailSuccess(ModelForDutyDetail dutyListView) {
        AppController.getInstance().hideProgressDialog();
        setDutyData(dutyListView);
    }

    private void actionType(ModelForDutyDetail dutyListView) {
        switch (dutyListView.getState()) {
            case Constants.START:
                action = Constants.IN_PROGRESS;
                break;
            case Constants.IN_PROGRESS:
                action = Constants.COMPLETE;
                break;
            case Constants.PLANNED:
                action = Constants.START;
                break;
            case Constants.COMPLETED:
                action = "";
                break;
        }
    }

    @Override
    public void getDutyDetailError(String message) {
        AppController.getInstance().hideProgressDialog();
        DialogUtils.showDialog(getActivity(), message).show();
    }

    @Override
    public void showProgressBar() {
        AppController.getInstance().showProgressDialog(getActivity());
    }

    @Override
    public void updateDutySuccess(ModelForDutyDetail dutyListView) {
        AppController.getInstance().hideProgressDialog();
        setDutyData(dutyListView);
    }

    private void setDutyData(ModelForDutyDetail dutyListView) {
        assigned = dutyListView.getAssigned();
        type = dutyListView.getType();
        id = String.valueOf(dutyListView.getId());
        txtforassigned.setText(assigned);
        txtforid.setText(id);
        txtforstate.setText(dutyListView.getState());
        txtfortype.setText(type);
        actionType(dutyListView);
    }

    @OnClick(R.id.layUpdate)
    public void onViewClicked() {
        if (!action.equals("")) {
            DialogUtils.showDialog(getActivity(), getResources().getString(R.string.update), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    timestamp = System.currentTimeMillis();
                    detailPersenterMvp.showProgressBar();
                    detailPersenterMvp.updateDuty(id, rawData(action, assigned, latitude, longitude, timestamp), GenrateChecksum.checksumForUpdateDuty(id, action, assigned, latitude, longitude, timestamp, Constants.USER));
                }
            }).show();
        } else {
            DialogUtils.showDialog(getActivity(), getResources().getString(R.string.complete)).show();
        }

    }

    private Map<String, String> rawData(String action, String assigned, Double latitude, Double longitude, long timestamp) {
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("action", action);
        requestBody.put("assigned", assigned);
        requestBody.put("latitude", String.valueOf(latitude));
        requestBody.put("longitude", String.valueOf(longitude));
        requestBody.put("timestamp", String.valueOf(timestamp));
        requestBody.put("user", Constants.USER);
        return requestBody;
    }

    @Override
    public void onResume() {
        super.onResume();
        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(getActivity());
        }
        location.beginUpdates();
    }

    @Override
    public void onPause() {
        // stop location updates (saves battery)
        location.endUpdates();
        super.onPause();
    }

}
