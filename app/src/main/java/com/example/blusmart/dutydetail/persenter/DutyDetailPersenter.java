package com.example.blusmart.dutydetail.persenter;

import java.util.Map;

@SuppressWarnings("ALL")
interface DutyDetailPersenter {
    void getDutyDetail(String id, String checksum);

    void showProgressBar();

    void updateDuty(String id, Map<String, String> requestBody,String checksumforUpdate);

}
