package com.example.blusmart.dutydetail.view;

import com.example.blusmart.dutydetail.model.ModelForDutyDetail;

public interface DutyDetailView {
    void getDutyDetailSuccess(ModelForDutyDetail dutyListView);

    void getDutyDetailError(String message);

    void showProgressBar();

    void updateDutySuccess(ModelForDutyDetail dutyListView);
}
