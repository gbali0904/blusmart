package com.example.blusmart.dutydetail.persenter;

import com.example.blusmart.app.AppController;
import com.example.blusmart.dutydetail.model.ModelForDutyDetail;
import com.example.blusmart.dutydetail.view.DutyDetailView;

import java.util.Map;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

@SuppressWarnings("ALL")
public class DutyDetailPersenterMvp implements DutyDetailPersenter {
    private final DutyDetailView dutyDetailView;

    public DutyDetailPersenterMvp(DutyDetailView dutyDetailView) {
        this.dutyDetailView=dutyDetailView;
    }

    @Override
    public void getDutyDetail(String id, String checksum) {
        AppController.getInstance().getRetrofitServices().getDutyDetail(id,"no-cache",checksum)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AppController.getInstance().defaultSubscribeScheduler())
                .subscribe(new Subscriber<ModelForDutyDetail>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        dutyDetailView.getDutyDetailError(e.getMessage());
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext( ModelForDutyDetail   dutyListView) {
                        AppController.getInstance().hideProgressDialog();
                        dutyDetailView.getDutyDetailSuccess(dutyListView);
                    }
                });
    }

    @Override
    public void showProgressBar() {
        dutyDetailView.showProgressBar();
    }

    @Override
    public void updateDuty(String id,  Map<String, String> requestBody, String checksumforUpdate) {
        AppController.getInstance().getRetrofitServices().updateDuty(id,"no-cache",checksumforUpdate,requestBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AppController.getInstance().defaultSubscribeScheduler())
                .subscribe(new Subscriber<ModelForDutyDetail>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        dutyDetailView.getDutyDetailError(e.getMessage());
                        e.printStackTrace();
                    }
                    @Override
                    public void onNext( ModelForDutyDetail dutyListView) {
                        AppController.getInstance().hideProgressDialog();
                        dutyDetailView.updateDutySuccess(dutyListView);
                    }
                });
    }
}
