package com.example.blusmart.splash;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.blusmart.R;
import com.example.blusmart.main.MainActivity;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import me.tankery.permission.PermissionRequestActivity;

public class SplashActivity extends AppCompatActivity {
    private static final String[] MUST_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int REQUEST_CODE = 121;
    private static final int Splash_TIME_DELEY = 2000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_main);
        ButterKnife.bind(this);
        checkPermissionALL();
    }
    private void checkPermissionALL() {
        PermissionRequestActivity.start(this, REQUEST_CODE,
                MUST_PERMISSIONS, getResources().getString(R.string.permission_rationale), getResources().getString(R.string.permission_rationale));
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                nextActivity();
            } else {
                checkPermissionALL();
            }
        }
    }

    private void nextActivity(){
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        };
        Timer t = new Timer();
        t.schedule(task, Splash_TIME_DELEY);
    }
}
