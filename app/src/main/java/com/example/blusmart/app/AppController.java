package com.example.blusmart.app;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.example.blusmart.R;
import com.example.blusmart.rest.ApiInterface;
import com.example.blusmart.utility.Constants;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.schedulers.Schedulers;

@SuppressWarnings("RedundantCast")
public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();
    private static AppController mInstance;
    private Dialog mProgressDlg;
    private Scheduler defaultSubscribeScheduler;
    private ProgressBar pv_circular_colors;
    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public ApiInterface getRetrofitServices() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES)
                .addInterceptor(logging);
        httpClient.addInterceptor(logging);  // <-- this is the important line!
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient.build())
                .build();
        ApiInterface apiInterface = retrofit.create(ApiInterface.class);
        return apiInterface;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }

    public void showProgressDialog(Context context) {
        mProgressDlg = new Dialog(context);
        mProgressDlg.setCancelable(false);
        mProgressDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        mProgressDlg.getWindow().setAttributes(lp);
        mProgressDlg.setContentView(R.layout.progress_dialog);
        pv_circular_colors = mProgressDlg.findViewById(R.id.pb_loading);
        pv_circular_colors.setVisibility(View.VISIBLE);
        mProgressDlg.show();

    }

    public void hideProgressDialog() {
        if (mProgressDlg != null) {
            if (mProgressDlg.isShowing())
                pv_circular_colors.setVisibility(View.GONE);
            mProgressDlg.dismiss();
            /*   mProgressDlg.dismiss();*/
        }

    }
}
